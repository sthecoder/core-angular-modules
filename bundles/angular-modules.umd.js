(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('@angular/animations'), require('@angular/platform-browser/animations'), require('@angular/common/http')) :
    typeof define === 'function' && define.amd ? define('angular-modules', ['exports', '@angular/core', '@angular/common', '@angular/animations', '@angular/platform-browser/animations', '@angular/common/http'], factory) :
    (factory((global['angular-modules'] = {}),global.ng.core,global.ng.common,global.ng.animations,global.ng.platformBrowser.animations,global.ng.common.http));
}(this, (function (exports,core,common,animations,animations$1,http) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var AutofocusDirective = (function () {
        function AutofocusDirective(_elementRef) {
            this._elementRef = _elementRef;
            this._el = this._elementRef.nativeElement;
        }
        /**
         * @return {?}
         */
        AutofocusDirective.prototype.ngAfterViewInit = /**
         * @return {?}
         */
            function () {
                this._el.focus();
            };
        AutofocusDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: '[acAutofocus]'
                    },] },
        ];
        /** @nocollapse */
        AutofocusDirective.ctorParameters = function () {
            return [
                { type: core.ElementRef, },
            ];
        };
        return AutofocusDirective;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var AutofocusModule = (function () {
        function AutofocusModule() {
        }
        AutofocusModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            common.CommonModule
                        ],
                        declarations: [AutofocusDirective],
                        exports: [AutofocusDirective]
                    },] },
        ];
        return AutofocusModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var CollapseComponent = (function () {
        function CollapseComponent() {
        }
        /**
         * @return {?}
         */
        CollapseComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () { };
        CollapseComponent.decorators = [
            { type: core.Component, args: [{
                        selector: '[acCollapse]',
                        template: "<ng-content></ng-content>",
                        animations: [
                            animations.trigger('collapse', [
                                animations.state('0', animations.style({ height: '0', opacity: '0', overflow: 'auto' })),
                                animations.state('1', animations.style({ height: '*', opacity: '1' })),
                                animations.transition('0 => 1', animations.animate('250ms ease-in')),
                                animations.transition('1 => 0', animations.animate('250ms ease-out'))
                            ])
                        ]
                    },] },
        ];
        /** @nocollapse */
        CollapseComponent.ctorParameters = function () { return []; };
        CollapseComponent.propDecorators = {
            "acCollapse": [{ type: core.HostBinding, args: ['@collapse',] }, { type: core.Input },],
        };
        return CollapseComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var CollapseModule = (function () {
        function CollapseModule() {
        }
        CollapseModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            common.CommonModule,
                            animations$1.BrowserAnimationsModule
                        ],
                        declarations: [CollapseComponent],
                        exports: [CollapseComponent]
                    },] },
        ];
        return CollapseModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var UrlHttpClientService = (function () {
        function UrlHttpClientService(_httpClient) {
            this._httpClient = _httpClient;
        }
        /**
         * @template T
         * @param {?} resourceUrl
         * @param {?=} options
         * @return {?}
         */
        UrlHttpClientService.prototype.get = /**
         * @template T
         * @param {?} resourceUrl
         * @param {?=} options
         * @return {?}
         */
            function (resourceUrl, options) {
                if (options === void 0) {
                    options = {};
                }
                return this._httpClient.get(this._getFullResourceUrl(resourceUrl), options);
            };
        // noinspection JSUnusedGlobalSymbols
        /**
         * @template T
         * @param {?} resourceUrl
         * @param {?=} options
         * @return {?}
         */
        UrlHttpClientService.prototype.delete = /**
         * @template T
         * @param {?} resourceUrl
         * @param {?=} options
         * @return {?}
         */
            function (resourceUrl, options) {
                return this._httpClient.delete(this._getFullResourceUrl(resourceUrl), options);
            };
        /**
         * @template T
         * @param {?} resourceUrl
         * @param {?=} options
         * @return {?}
         */
        UrlHttpClientService.prototype.options = /**
         * @template T
         * @param {?} resourceUrl
         * @param {?=} options
         * @return {?}
         */
            function (resourceUrl, options) {
                return this._httpClient.options(this._getFullResourceUrl(resourceUrl), options);
            };
        /**
         * @template T
         * @param {?} resourceUrl
         * @param {?} body
         * @param {?=} options
         * @return {?}
         */
        UrlHttpClientService.prototype.patch = /**
         * @template T
         * @param {?} resourceUrl
         * @param {?} body
         * @param {?=} options
         * @return {?}
         */
            function (resourceUrl, body, options) {
                if (options === void 0) {
                    options = {};
                }
                return this._httpClient.patch(this._getFullResourceUrl(resourceUrl), body, options);
            };
        /**
         * @template T
         * @param {?} resourceUrl
         * @param {?} body
         * @param {?=} options
         * @return {?}
         */
        UrlHttpClientService.prototype.post = /**
         * @template T
         * @param {?} resourceUrl
         * @param {?} body
         * @param {?=} options
         * @return {?}
         */
            function (resourceUrl, body, options) {
                if (options === void 0) {
                    options = {};
                }
                return this._httpClient.post(this._getFullResourceUrl(resourceUrl), body, options);
            };
        /**
         * @template T
         * @param {?} resourceUrl
         * @param {?} body
         * @param {?=} options
         * @return {?}
         */
        UrlHttpClientService.prototype.update = /**
         * @template T
         * @param {?} resourceUrl
         * @param {?} body
         * @param {?=} options
         * @return {?}
         */
            function (resourceUrl, body, options) {
                if (options === void 0) {
                    options = {};
                }
                return this._httpClient.put(this._getFullResourceUrl(resourceUrl), body, options);
            };
        /**
         * @param {?} baseAPI
         * @return {?}
         */
        UrlHttpClientService.prototype.setBaseAPI = /**
         * @param {?} baseAPI
         * @return {?}
         */
            function (baseAPI) {
                this._base_api = baseAPI;
            };
        /**
         * @param {?} resourceUrl
         * @return {?}
         */
        UrlHttpClientService.prototype._getFullResourceUrl = /**
         * @param {?} resourceUrl
         * @return {?}
         */
            function (resourceUrl) {
                if (resourceUrl[0] === '/') {
                    return this._base_api + resourceUrl;
                }
                else {
                    return this._base_api + '/' + resourceUrl;
                }
            };
        UrlHttpClientService.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        UrlHttpClientService.ctorParameters = function () {
            return [
                { type: http.HttpClient, },
            ];
        };
        return UrlHttpClientService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var UrlHttpClientModule = (function () {
        function UrlHttpClientModule() {
        }
        UrlHttpClientModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            common.CommonModule,
                            http.HttpClientModule
                        ],
                        declarations: [],
                        providers: [
                            UrlHttpClientService
                        ]
                    },] },
        ];
        return UrlHttpClientModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    exports.AutofocusModule = AutofocusModule;
    exports.AutofocusDirective = AutofocusDirective;
    exports.CollapseModule = CollapseModule;
    exports.CollapseComponent = CollapseComponent;
    exports.UrlHttpClientModule = UrlHttpClientModule;
    exports.UrlHttpClientService = UrlHttpClientService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1tb2R1bGVzLnVtZC5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vYW5ndWxhci1tb2R1bGVzL3NyYy9saWIvbW9kdWxlcy9hdXRvZm9jdXMvYXV0b2ZvY3VzLmRpcmVjdGl2ZS50cyIsIm5nOi8vYW5ndWxhci1tb2R1bGVzL3NyYy9saWIvbW9kdWxlcy9hdXRvZm9jdXMvYXV0b2ZvY3VzLm1vZHVsZS50cyIsIm5nOi8vYW5ndWxhci1tb2R1bGVzL3NyYy9saWIvbW9kdWxlcy9jb2xsYXBzZS9jb2xsYXBzZS9jb2xsYXBzZS5jb21wb25lbnQudHMiLCJuZzovL2FuZ3VsYXItbW9kdWxlcy9zcmMvbGliL21vZHVsZXMvY29sbGFwc2UvY29sbGFwc2UubW9kdWxlLnRzIiwibmc6Ly9hbmd1bGFyLW1vZHVsZXMvc3JjL2xpYi9tb2R1bGVzL3VybC1odHRwLWNsaWVudC91cmwtaHR0cC1jbGllbnQuc2VydmljZS50cyIsIm5nOi8vYW5ndWxhci1tb2R1bGVzL3NyYy9saWIvbW9kdWxlcy91cmwtaHR0cC1jbGllbnQvdXJsLWh0dHAtY2xpZW50Lm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBZnRlclZpZXdJbml0LCBEaXJlY3RpdmUsIEVsZW1lbnRSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW2FjQXV0b2ZvY3VzXSdcbn0pXG5leHBvcnQgY2xhc3MgQXV0b2ZvY3VzRGlyZWN0aXZlIGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XG4gIHByaXZhdGUgX2VsOiBhbnk7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfZWxlbWVudFJlZjogRWxlbWVudFJlZikge1xuICAgIHRoaXMuX2VsID0gdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50O1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIHRoaXMuX2VsLmZvY3VzKCk7XG4gIH1cbn1cbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5pbXBvcnQgeyBBdXRvZm9jdXNEaXJlY3RpdmUgfSBmcm9tICcuL2F1dG9mb2N1cy5kaXJlY3RpdmUnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW0F1dG9mb2N1c0RpcmVjdGl2ZV0sXG4gIGV4cG9ydHM6IFtBdXRvZm9jdXNEaXJlY3RpdmVdXG59KVxuZXhwb3J0IGNsYXNzIEF1dG9mb2N1c01vZHVsZSB7fVxuIiwiaW1wb3J0IHtDb21wb25lbnQsIEhvc3RCaW5kaW5nLCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7YW5pbWF0ZSwgc3RhdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCB0cmlnZ2VyfSBmcm9tIFwiQGFuZ3VsYXIvYW5pbWF0aW9uc1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdbYWNDb2xsYXBzZV0nLFxuICB0ZW1wbGF0ZTogYDxuZy1jb250ZW50PjwvbmctY29udGVudD5gLFxuICBhbmltYXRpb25zOiBbXG4gICAgdHJpZ2dlcignY29sbGFwc2UnLCBbXG4gICAgICBzdGF0ZSgnMCcsIHN0eWxlKHsgaGVpZ2h0OiAnMCcsIG9wYWNpdHk6ICcwJywgb3ZlcmZsb3c6ICdhdXRvJyB9KSksXG4gICAgICBzdGF0ZSgnMScsIHN0eWxlKHsgaGVpZ2h0OiAnKicsIG9wYWNpdHk6ICcxJyB9KSksXG4gICAgICB0cmFuc2l0aW9uKCcwID0+IDEnLCBhbmltYXRlKCcyNTBtcyBlYXNlLWluJykpLFxuICAgICAgdHJhbnNpdGlvbignMSA9PiAwJywgYW5pbWF0ZSgnMjUwbXMgZWFzZS1vdXQnKSlcbiAgICBdKVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIENvbGxhcHNlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQEhvc3RCaW5kaW5nKCdAY29sbGFwc2UnKVxuICBASW5wdXQoKVxuICBhY0NvbGxhcHNlOiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBuZ09uSW5pdCgpIHt9XG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IENvbGxhcHNlQ29tcG9uZW50IH0gZnJvbSAnLi9jb2xsYXBzZS9jb2xsYXBzZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQnJvd3NlckFuaW1hdGlvbnNNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlci9hbmltYXRpb25zXCI7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgQnJvd3NlckFuaW1hdGlvbnNNb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbQ29sbGFwc2VDb21wb25lbnRdLFxuICBleHBvcnRzOiBbQ29sbGFwc2VDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIENvbGxhcHNlTW9kdWxlIHt9XG4iLCJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtIdHRwQ2xpZW50fSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge0h0dHBFdmVudH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwL3NyYy9yZXNwb25zZVwiO1xuaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tIFwicnhqc1wiO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgVXJsSHR0cENsaWVudFNlcnZpY2Uge1xuICBwcml2YXRlIF9iYXNlX2FwaTogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgX2h0dHBDbGllbnQ6IEh0dHBDbGllbnRcbiAgKSB7fVxuXG4gIGdldDxUPihyZXNvdXJjZVVybDogc3RyaW5nLCBvcHRpb25zID0ge30pOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICByZXR1cm4gdGhpcy5faHR0cENsaWVudC5nZXQ8VD4odGhpcy5fZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsKSwgb3B0aW9ucyk7XG4gIH1cblxuICAvLyBub2luc3BlY3Rpb24gSlNVbnVzZWRHbG9iYWxTeW1ib2xzXG4gIGRlbGV0ZTxUPihyZXNvdXJjZVVybDogc3RyaW5nLCBvcHRpb25zPyk6IE9ic2VydmFibGU8SHR0cEV2ZW50PFQ+PiB7XG4gICAgcmV0dXJuIHRoaXMuX2h0dHBDbGllbnQuZGVsZXRlPFQ+KHRoaXMuX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybCksIG9wdGlvbnMpO1xuICB9XG5cbiAgb3B0aW9uczxUPihyZXNvdXJjZVVybDogc3RyaW5nLCBvcHRpb25zPyk6IE9ic2VydmFibGU8SHR0cEV2ZW50PFQ+PiB7XG4gICAgcmV0dXJuIHRoaXMuX2h0dHBDbGllbnQub3B0aW9uczxUPih0aGlzLl9nZXRGdWxsUmVzb3VyY2VVcmwocmVzb3VyY2VVcmwpLCBvcHRpb25zKTtcbiAgfVxuXG4gIHBhdGNoPFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIGJvZHk6IGFueSB8IG51bGwsIG9wdGlvbnMgPSB7fSk6IE9ic2VydmFibGU8VD4ge1xuICAgIHJldHVybiB0aGlzLl9odHRwQ2xpZW50LnBhdGNoPFQ+KHRoaXMuX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybCksIGJvZHksIG9wdGlvbnMpO1xuICB9XG5cbiAgcG9zdDxUPihyZXNvdXJjZVVybDogc3RyaW5nLCBib2R5OiBhbnkgfCBudWxsLCBvcHRpb25zID0ge30pOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICByZXR1cm4gdGhpcy5faHR0cENsaWVudC5wb3N0PFQ+KHRoaXMuX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybCksIGJvZHksIG9wdGlvbnMpO1xuICB9XG5cbiAgdXBkYXRlPFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIGJvZHk6IGFueSB8IG51bGwsIG9wdGlvbnMgPSB7fSk6IE9ic2VydmFibGU8VD4ge1xuICAgIHJldHVybiB0aGlzLl9odHRwQ2xpZW50LnB1dDxUPih0aGlzLl9nZXRGdWxsUmVzb3VyY2VVcmwocmVzb3VyY2VVcmwpLCBib2R5LCBvcHRpb25zKTtcbiAgfVxuXG4gIHNldEJhc2VBUEkoYmFzZUFQSSk6IHZvaWQge1xuICAgIHRoaXMuX2Jhc2VfYXBpID0gYmFzZUFQSTtcbiAgfVxuXG4gIC8qUHJpdmF0ZSBNZXRob2RzKi9cbiAgcHJpdmF0ZSBfZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsOiBzdHJpbmcpIHtcbiAgICBpZiAocmVzb3VyY2VVcmxbMF0gPT09ICcvJykge1xuICAgICAgcmV0dXJuIHRoaXMuX2Jhc2VfYXBpICsgcmVzb3VyY2VVcmw7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB0aGlzLl9iYXNlX2FwaSArICcvJyArIHJlc291cmNlVXJsO1xuICAgIH1cbiAgfVxufVxuIiwiaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge0h0dHBDbGllbnRNb2R1bGV9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xuaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1VybEh0dHBDbGllbnRTZXJ2aWNlfSBmcm9tICcuL3VybC1odHRwLWNsaWVudC5zZXJ2aWNlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBIdHRwQ2xpZW50TW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW10sXG4gIHByb3ZpZGVyczogW1xuICAgIFVybEh0dHBDbGllbnRTZXJ2aWNlXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgVXJsSHR0cENsaWVudE1vZHVsZSB7fVxuIl0sIm5hbWVzIjpbIkRpcmVjdGl2ZSIsIkVsZW1lbnRSZWYiLCJOZ01vZHVsZSIsIkNvbW1vbk1vZHVsZSIsIkNvbXBvbmVudCIsInRyaWdnZXIiLCJzdGF0ZSIsInN0eWxlIiwidHJhbnNpdGlvbiIsImFuaW1hdGUiLCJIb3N0QmluZGluZyIsIklucHV0IiwiQnJvd3NlckFuaW1hdGlvbnNNb2R1bGUiLCJJbmplY3RhYmxlIiwiSHR0cENsaWVudCIsIkh0dHBDbGllbnRNb2R1bGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtRQVFFLDRCQUFvQixXQUF1QjtZQUF2QixnQkFBVyxHQUFYLFdBQVcsQ0FBWTtZQUN6QyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDO1NBQzNDOzs7O1FBRUQsNENBQWU7OztZQUFmO2dCQUNFLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDbEI7O29CQVpGQSxjQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLGVBQWU7cUJBQzFCOzs7Ozt3QkFKa0NDLGVBQVU7OztpQ0FBN0M7Ozs7Ozs7QUNBQTs7OztvQkFLQ0MsYUFBUSxTQUFDO3dCQUNSLE9BQU8sRUFBRTs0QkFDUEMsbUJBQVk7eUJBQ2I7d0JBQ0QsWUFBWSxFQUFFLENBQUMsa0JBQWtCLENBQUM7d0JBQ2xDLE9BQU8sRUFBRSxDQUFDLGtCQUFrQixDQUFDO3FCQUM5Qjs7OEJBWEQ7Ozs7Ozs7QUNBQTtRQW9CRTtTQUFnQjs7OztRQUVoQixvQ0FBUTs7O1lBQVIsZUFBYTs7b0JBbkJkQyxjQUFTLFNBQUM7d0JBQ1QsUUFBUSxFQUFFLGNBQWM7d0JBQ3hCLFFBQVEsRUFBRSwyQkFBMkI7d0JBQ3JDLFVBQVUsRUFBRTs0QkFDVkMsa0JBQU8sQ0FBQyxVQUFVLEVBQUU7Z0NBQ2xCQyxnQkFBSyxDQUFDLEdBQUcsRUFBRUMsZ0JBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztnQ0FDbEVELGdCQUFLLENBQUMsR0FBRyxFQUFFQyxnQkFBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztnQ0FDaERDLHFCQUFVLENBQUMsUUFBUSxFQUFFQyxrQkFBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dDQUM5Q0QscUJBQVUsQ0FBQyxRQUFRLEVBQUVDLGtCQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzs2QkFDaEQsQ0FBQzt5QkFDSDtxQkFDRjs7Ozs7bUNBRUVDLGdCQUFXLFNBQUMsV0FBVyxjQUN2QkMsVUFBSzs7Z0NBakJSOzs7Ozs7O0FDQUE7Ozs7b0JBS0NULGFBQVEsU0FBQzt3QkFDUixPQUFPLEVBQUU7NEJBQ1BDLG1CQUFZOzRCQUNaUyxvQ0FBdUI7eUJBQ3hCO3dCQUNELFlBQVksRUFBRSxDQUFDLGlCQUFpQixDQUFDO3dCQUNqQyxPQUFPLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztxQkFDN0I7OzZCQVpEOzs7Ozs7O0FDQUE7UUFTRSw4QkFDVTtZQUFBLGdCQUFXLEdBQVgsV0FBVztTQUNqQjs7Ozs7OztRQUVKLGtDQUFHOzs7Ozs7WUFBSCxVQUFPLFdBQW1CLEVBQUUsT0FBWTtnQkFBWix3QkFBQTtvQkFBQSxZQUFZOztnQkFDdEMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7YUFDaEY7Ozs7Ozs7O1FBR0QscUNBQU07Ozs7OztZQUFOLFVBQVUsV0FBbUIsRUFBRSxPQUFRO2dCQUNyQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQzthQUNuRjs7Ozs7OztRQUVELHNDQUFPOzs7Ozs7WUFBUCxVQUFXLFdBQW1CLEVBQUUsT0FBUTtnQkFDdEMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7YUFDcEY7Ozs7Ozs7O1FBRUQsb0NBQUs7Ozs7Ozs7WUFBTCxVQUFTLFdBQW1CLEVBQUUsSUFBZ0IsRUFBRSxPQUFZO2dCQUFaLHdCQUFBO29CQUFBLFlBQVk7O2dCQUMxRCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7YUFDeEY7Ozs7Ozs7O1FBRUQsbUNBQUk7Ozs7Ozs7WUFBSixVQUFRLFdBQW1CLEVBQUUsSUFBZ0IsRUFBRSxPQUFZO2dCQUFaLHdCQUFBO29CQUFBLFlBQVk7O2dCQUN6RCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7YUFDdkY7Ozs7Ozs7O1FBRUQscUNBQU07Ozs7Ozs7WUFBTixVQUFVLFdBQW1CLEVBQUUsSUFBZ0IsRUFBRSxPQUFZO2dCQUFaLHdCQUFBO29CQUFBLFlBQVk7O2dCQUMzRCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7YUFDdEY7Ozs7O1FBRUQseUNBQVU7Ozs7WUFBVixVQUFXLE9BQU87Z0JBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO2FBQzFCOzs7OztRQUdPLGtEQUFtQjs7OztzQkFBQyxXQUFtQjtnQkFDN0MsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO29CQUMxQixPQUFPLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDO2lCQUNyQztxQkFBTTtvQkFDTCxPQUFPLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQztpQkFDM0M7OztvQkEzQ0pDLGVBQVU7Ozs7O3dCQUpIQyxlQUFVOzs7bUNBRGxCOzs7Ozs7O0FDQUE7Ozs7b0JBS0NaLGFBQVEsU0FBQzt3QkFDUixPQUFPLEVBQUU7NEJBQ1BDLG1CQUFZOzRCQUNaWSxxQkFBZ0I7eUJBQ2pCO3dCQUNELFlBQVksRUFBRSxFQUFFO3dCQUNoQixTQUFTLEVBQUU7NEJBQ1Qsb0JBQW9CO3lCQUNyQjtxQkFDRjs7a0NBZEQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsifQ==