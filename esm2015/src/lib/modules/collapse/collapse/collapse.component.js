/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, HostBinding, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from "@angular/animations";
export class CollapseComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
CollapseComponent.decorators = [
    { type: Component, args: [{
                selector: '[acCollapse]',
                template: `<ng-content></ng-content>`,
                animations: [
                    trigger('collapse', [
                        state('0', style({ height: '0', opacity: '0', overflow: 'auto' })),
                        state('1', style({ height: '*', opacity: '1' })),
                        transition('0 => 1', animate('250ms ease-in')),
                        transition('1 => 0', animate('250ms ease-out'))
                    ])
                ]
            },] },
];
/** @nocollapse */
CollapseComponent.ctorParameters = () => [];
CollapseComponent.propDecorators = {
    "acCollapse": [{ type: HostBinding, args: ['@collapse',] }, { type: Input },],
};
function CollapseComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    CollapseComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    CollapseComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    CollapseComponent.propDecorators;
    /** @type {?} */
    CollapseComponent.prototype.acCollapse;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1tb2R1bGVzLyIsInNvdXJjZXMiOlsic3JjL2xpYi9tb2R1bGVzL2NvbGxhcHNlL2NvbGxhcHNlL2NvbGxhcHNlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3BFLE9BQU8sRUFBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFjL0UsTUFBTTtJQUtKLGlCQUFnQjs7OztJQUVoQixRQUFRLE1BQUs7OztZQW5CZCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLFFBQVEsRUFBRSwyQkFBMkI7Z0JBQ3JDLFVBQVUsRUFBRTtvQkFDVixPQUFPLENBQUMsVUFBVSxFQUFFO3dCQUNsQixLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQzt3QkFDbEUsS0FBSyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNoRCxVQUFVLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxlQUFlLENBQUMsQ0FBQzt3QkFDOUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztxQkFDaEQsQ0FBQztpQkFDSDthQUNGOzs7OzsyQkFFRSxXQUFXLFNBQUMsV0FBVyxjQUN2QixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIEhvc3RCaW5kaW5nLCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7YW5pbWF0ZSwgc3RhdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCB0cmlnZ2VyfSBmcm9tIFwiQGFuZ3VsYXIvYW5pbWF0aW9uc1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdbYWNDb2xsYXBzZV0nLFxuICB0ZW1wbGF0ZTogYDxuZy1jb250ZW50PjwvbmctY29udGVudD5gLFxuICBhbmltYXRpb25zOiBbXG4gICAgdHJpZ2dlcignY29sbGFwc2UnLCBbXG4gICAgICBzdGF0ZSgnMCcsIHN0eWxlKHsgaGVpZ2h0OiAnMCcsIG9wYWNpdHk6ICcwJywgb3ZlcmZsb3c6ICdhdXRvJyB9KSksXG4gICAgICBzdGF0ZSgnMScsIHN0eWxlKHsgaGVpZ2h0OiAnKicsIG9wYWNpdHk6ICcxJyB9KSksXG4gICAgICB0cmFuc2l0aW9uKCcwID0+IDEnLCBhbmltYXRlKCcyNTBtcyBlYXNlLWluJykpLFxuICAgICAgdHJhbnNpdGlvbignMSA9PiAwJywgYW5pbWF0ZSgnMjUwbXMgZWFzZS1vdXQnKSlcbiAgICBdKVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIENvbGxhcHNlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQEhvc3RCaW5kaW5nKCdAY29sbGFwc2UnKVxuICBASW5wdXQoKVxuICBhY0NvbGxhcHNlOiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBuZ09uSW5pdCgpIHt9XG59XG4iXX0=