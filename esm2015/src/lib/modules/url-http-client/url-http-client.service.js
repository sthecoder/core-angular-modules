/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
export class UrlHttpClientService {
    /**
     * @param {?} _httpClient
     */
    constructor(_httpClient) {
        this._httpClient = _httpClient;
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    get(resourceUrl, options = {}) {
        return this._httpClient.get(this._getFullResourceUrl(resourceUrl), options);
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    delete(resourceUrl, options) {
        return this._httpClient.delete(this._getFullResourceUrl(resourceUrl), options);
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    options(resourceUrl, options) {
        return this._httpClient.options(this._getFullResourceUrl(resourceUrl), options);
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    patch(resourceUrl, body, options = {}) {
        return this._httpClient.patch(this._getFullResourceUrl(resourceUrl), body, options);
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    post(resourceUrl, body, options = {}) {
        return this._httpClient.post(this._getFullResourceUrl(resourceUrl), body, options);
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    update(resourceUrl, body, options = {}) {
        return this._httpClient.put(this._getFullResourceUrl(resourceUrl), body, options);
    }
    /**
     * @param {?} baseAPI
     * @return {?}
     */
    setBaseAPI(baseAPI) {
        this._base_api = baseAPI;
    }
    /**
     * @param {?} resourceUrl
     * @return {?}
     */
    _getFullResourceUrl(resourceUrl) {
        if (resourceUrl[0] === '/') {
            return this._base_api + resourceUrl;
        }
        else {
            return this._base_api + '/' + resourceUrl;
        }
    }
}
UrlHttpClientService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
UrlHttpClientService.ctorParameters = () => [
    { type: HttpClient, },
];
function UrlHttpClientService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UrlHttpClientService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UrlHttpClientService.ctorParameters;
    /** @type {?} */
    UrlHttpClientService.prototype._base_api;
    /** @type {?} */
    UrlHttpClientService.prototype._httpClient;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXJsLWh0dHAtY2xpZW50LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hbmd1bGFyLW1vZHVsZXMvIiwic291cmNlcyI6WyJzcmMvbGliL21vZHVsZXMvdXJsLWh0dHAtY2xpZW50L3VybC1odHRwLWNsaWVudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUtoRCxNQUFNOzs7O0lBR0osWUFDVTtRQUFBLGdCQUFXLEdBQVgsV0FBVztLQUNqQjs7Ozs7OztJQUVKLEdBQUcsQ0FBSSxXQUFtQixFQUFFLE9BQU8sR0FBRyxFQUFFO1FBQ3RDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7S0FDaEY7Ozs7Ozs7SUFHRCxNQUFNLENBQUksV0FBbUIsRUFBRSxPQUFRO1FBQ3JDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7S0FDbkY7Ozs7Ozs7SUFFRCxPQUFPLENBQUksV0FBbUIsRUFBRSxPQUFRO1FBQ3RDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7S0FDcEY7Ozs7Ozs7O0lBRUQsS0FBSyxDQUFJLFdBQW1CLEVBQUUsSUFBZ0IsRUFBRSxPQUFPLEdBQUcsRUFBRTtRQUMxRCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztLQUN4Rjs7Ozs7Ozs7SUFFRCxJQUFJLENBQUksV0FBbUIsRUFBRSxJQUFnQixFQUFFLE9BQU8sR0FBRyxFQUFFO1FBQ3pELE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3ZGOzs7Ozs7OztJQUVELE1BQU0sQ0FBSSxXQUFtQixFQUFFLElBQWdCLEVBQUUsT0FBTyxHQUFHLEVBQUU7UUFDM0QsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7S0FDdEY7Ozs7O0lBRUQsVUFBVSxDQUFDLE9BQU87UUFDaEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7S0FDMUI7Ozs7O0lBR08sbUJBQW1CLENBQUMsV0FBbUI7UUFDN0MsRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDM0IsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDO1NBQ3JDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLEdBQUcsV0FBVyxDQUFDO1NBQzNDOzs7O1lBM0NKLFVBQVU7Ozs7WUFKSCxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7SHR0cENsaWVudH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHtIdHRwRXZlbnR9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cC9zcmMvcmVzcG9uc2VcIjtcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSBcInJ4anNcIjtcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFVybEh0dHBDbGllbnRTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBfYmFzZV9hcGk6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIF9odHRwQ2xpZW50OiBIdHRwQ2xpZW50XG4gICkge31cblxuICBnZXQ8VD4ocmVzb3VyY2VVcmw6IHN0cmluZywgb3B0aW9ucyA9IHt9KTogT2JzZXJ2YWJsZTxUPiB7XG4gICAgcmV0dXJuIHRoaXMuX2h0dHBDbGllbnQuZ2V0PFQ+KHRoaXMuX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybCksIG9wdGlvbnMpO1xuICB9XG5cbiAgLy8gbm9pbnNwZWN0aW9uIEpTVW51c2VkR2xvYmFsU3ltYm9sc1xuICBkZWxldGU8VD4ocmVzb3VyY2VVcmw6IHN0cmluZywgb3B0aW9ucz8pOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxUPj4ge1xuICAgIHJldHVybiB0aGlzLl9odHRwQ2xpZW50LmRlbGV0ZTxUPih0aGlzLl9nZXRGdWxsUmVzb3VyY2VVcmwocmVzb3VyY2VVcmwpLCBvcHRpb25zKTtcbiAgfVxuXG4gIG9wdGlvbnM8VD4ocmVzb3VyY2VVcmw6IHN0cmluZywgb3B0aW9ucz8pOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxUPj4ge1xuICAgIHJldHVybiB0aGlzLl9odHRwQ2xpZW50Lm9wdGlvbnM8VD4odGhpcy5fZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsKSwgb3B0aW9ucyk7XG4gIH1cblxuICBwYXRjaDxUPihyZXNvdXJjZVVybDogc3RyaW5nLCBib2R5OiBhbnkgfCBudWxsLCBvcHRpb25zID0ge30pOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICByZXR1cm4gdGhpcy5faHR0cENsaWVudC5wYXRjaDxUPih0aGlzLl9nZXRGdWxsUmVzb3VyY2VVcmwocmVzb3VyY2VVcmwpLCBib2R5LCBvcHRpb25zKTtcbiAgfVxuXG4gIHBvc3Q8VD4ocmVzb3VyY2VVcmw6IHN0cmluZywgYm9keTogYW55IHwgbnVsbCwgb3B0aW9ucyA9IHt9KTogT2JzZXJ2YWJsZTxUPiB7XG4gICAgcmV0dXJuIHRoaXMuX2h0dHBDbGllbnQucG9zdDxUPih0aGlzLl9nZXRGdWxsUmVzb3VyY2VVcmwocmVzb3VyY2VVcmwpLCBib2R5LCBvcHRpb25zKTtcbiAgfVxuXG4gIHVwZGF0ZTxUPihyZXNvdXJjZVVybDogc3RyaW5nLCBib2R5OiBhbnkgfCBudWxsLCBvcHRpb25zID0ge30pOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICByZXR1cm4gdGhpcy5faHR0cENsaWVudC5wdXQ8VD4odGhpcy5fZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsKSwgYm9keSwgb3B0aW9ucyk7XG4gIH1cblxuICBzZXRCYXNlQVBJKGJhc2VBUEkpOiB2b2lkIHtcbiAgICB0aGlzLl9iYXNlX2FwaSA9IGJhc2VBUEk7XG4gIH1cblxuICAvKlByaXZhdGUgTWV0aG9kcyovXG4gIHByaXZhdGUgX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybDogc3RyaW5nKSB7XG4gICAgaWYgKHJlc291cmNlVXJsWzBdID09PSAnLycpIHtcbiAgICAgIHJldHVybiB0aGlzLl9iYXNlX2FwaSArIHJlc291cmNlVXJsO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gdGhpcy5fYmFzZV9hcGkgKyAnLycgKyByZXNvdXJjZVVybDtcbiAgICB9XG4gIH1cbn1cbiJdfQ==