/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Component, HostBinding, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from "@angular/animations";
var CollapseComponent = /** @class */ (function () {
    function CollapseComponent() {
    }
    /**
     * @return {?}
     */
    CollapseComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    CollapseComponent.decorators = [
        { type: Component, args: [{
                    selector: '[acCollapse]',
                    template: "<ng-content></ng-content>",
                    animations: [
                        trigger('collapse', [
                            state('0', style({ height: '0', opacity: '0', overflow: 'auto' })),
                            state('1', style({ height: '*', opacity: '1' })),
                            transition('0 => 1', animate('250ms ease-in')),
                            transition('1 => 0', animate('250ms ease-out'))
                        ])
                    ]
                },] },
    ];
    /** @nocollapse */
    CollapseComponent.ctorParameters = function () { return []; };
    CollapseComponent.propDecorators = {
        "acCollapse": [{ type: HostBinding, args: ['@collapse',] }, { type: Input },],
    };
    return CollapseComponent;
}());
export { CollapseComponent };
function CollapseComponent_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    CollapseComponent.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    CollapseComponent.ctorParameters;
    /** @type {!Object<string,!Array<{type: !Function, args: (undefined|!Array<?>)}>>} */
    CollapseComponent.propDecorators;
    /** @type {?} */
    CollapseComponent.prototype.acCollapse;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYW5ndWxhci1tb2R1bGVzLyIsInNvdXJjZXMiOlsic3JjL2xpYi9tb2R1bGVzL2NvbGxhcHNlL2NvbGxhcHNlL2NvbGxhcHNlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBQ3BFLE9BQU8sRUFBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFDLE1BQU0scUJBQXFCLENBQUM7O0lBbUI3RTtLQUFnQjs7OztJQUVoQixvQ0FBUTs7O0lBQVIsZUFBYTs7Z0JBbkJkLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsY0FBYztvQkFDeEIsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsVUFBVSxFQUFFO3dCQUNWLE9BQU8sQ0FBQyxVQUFVLEVBQUU7NEJBQ2xCLEtBQUssQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDOzRCQUNsRSxLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ2hELFVBQVUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDOzRCQUM5QyxVQUFVLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO3lCQUNoRCxDQUFDO3FCQUNIO2lCQUNGOzs7OzsrQkFFRSxXQUFXLFNBQUMsV0FBVyxjQUN2QixLQUFLOzs0QkFqQlI7O1NBZWEsaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIEhvc3RCaW5kaW5nLCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7YW5pbWF0ZSwgc3RhdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCB0cmlnZ2VyfSBmcm9tIFwiQGFuZ3VsYXIvYW5pbWF0aW9uc1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdbYWNDb2xsYXBzZV0nLFxuICB0ZW1wbGF0ZTogYDxuZy1jb250ZW50PjwvbmctY29udGVudD5gLFxuICBhbmltYXRpb25zOiBbXG4gICAgdHJpZ2dlcignY29sbGFwc2UnLCBbXG4gICAgICBzdGF0ZSgnMCcsIHN0eWxlKHsgaGVpZ2h0OiAnMCcsIG9wYWNpdHk6ICcwJywgb3ZlcmZsb3c6ICdhdXRvJyB9KSksXG4gICAgICBzdGF0ZSgnMScsIHN0eWxlKHsgaGVpZ2h0OiAnKicsIG9wYWNpdHk6ICcxJyB9KSksXG4gICAgICB0cmFuc2l0aW9uKCcwID0+IDEnLCBhbmltYXRlKCcyNTBtcyBlYXNlLWluJykpLFxuICAgICAgdHJhbnNpdGlvbignMSA9PiAwJywgYW5pbWF0ZSgnMjUwbXMgZWFzZS1vdXQnKSlcbiAgICBdKVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIENvbGxhcHNlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQEhvc3RCaW5kaW5nKCdAY29sbGFwc2UnKVxuICBASW5wdXQoKVxuICBhY0NvbGxhcHNlOiBib29sZWFuO1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBuZ09uSW5pdCgpIHt9XG59XG4iXX0=