/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
var UrlHttpClientService = /** @class */ (function () {
    function UrlHttpClientService(_httpClient) {
        this._httpClient = _httpClient;
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.get = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, options) {
        if (options === void 0) { options = {}; }
        return this._httpClient.get(this._getFullResourceUrl(resourceUrl), options);
    };
    // noinspection JSUnusedGlobalSymbols
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.delete = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, options) {
        return this._httpClient.delete(this._getFullResourceUrl(resourceUrl), options);
    };
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.options = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, options) {
        return this._httpClient.options(this._getFullResourceUrl(resourceUrl), options);
    };
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.patch = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, body, options) {
        if (options === void 0) { options = {}; }
        return this._httpClient.patch(this._getFullResourceUrl(resourceUrl), body, options);
    };
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.post = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, body, options) {
        if (options === void 0) { options = {}; }
        return this._httpClient.post(this._getFullResourceUrl(resourceUrl), body, options);
    };
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.update = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, body, options) {
        if (options === void 0) { options = {}; }
        return this._httpClient.put(this._getFullResourceUrl(resourceUrl), body, options);
    };
    /**
     * @param {?} baseAPI
     * @return {?}
     */
    UrlHttpClientService.prototype.setBaseAPI = /**
     * @param {?} baseAPI
     * @return {?}
     */
    function (baseAPI) {
        this._base_api = baseAPI;
    };
    /**
     * @param {?} resourceUrl
     * @return {?}
     */
    UrlHttpClientService.prototype._getFullResourceUrl = /**
     * @param {?} resourceUrl
     * @return {?}
     */
    function (resourceUrl) {
        if (resourceUrl[0] === '/') {
            return this._base_api + resourceUrl;
        }
        else {
            return this._base_api + '/' + resourceUrl;
        }
    };
    UrlHttpClientService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    UrlHttpClientService.ctorParameters = function () { return [
        { type: HttpClient, },
    ]; };
    return UrlHttpClientService;
}());
export { UrlHttpClientService };
function UrlHttpClientService_tsickle_Closure_declarations() {
    /** @type {!Array<{type: !Function, args: (undefined|!Array<?>)}>} */
    UrlHttpClientService.decorators;
    /**
     * @nocollapse
     * @type {function(): !Array<(null|{type: ?, decorators: (undefined|!Array<{type: !Function, args: (undefined|!Array<?>)}>)})>}
     */
    UrlHttpClientService.ctorParameters;
    /** @type {?} */
    UrlHttpClientService.prototype._base_api;
    /** @type {?} */
    UrlHttpClientService.prototype._httpClient;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXJsLWh0dHAtY2xpZW50LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hbmd1bGFyLW1vZHVsZXMvIiwic291cmNlcyI6WyJzcmMvbGliL21vZHVsZXMvdXJsLWh0dHAtY2xpZW50L3VybC1odHRwLWNsaWVudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQzs7SUFROUMsOEJBQ1U7UUFBQSxnQkFBVyxHQUFYLFdBQVc7S0FDakI7Ozs7Ozs7SUFFSixrQ0FBRzs7Ozs7O0lBQUgsVUFBTyxXQUFtQixFQUFFLE9BQVk7UUFBWix3QkFBQSxFQUFBLFlBQVk7UUFDdEMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztLQUNoRjtJQUVELHFDQUFxQzs7Ozs7OztJQUNyQyxxQ0FBTTs7Ozs7O0lBQU4sVUFBVSxXQUFtQixFQUFFLE9BQVE7UUFDckMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztLQUNuRjs7Ozs7OztJQUVELHNDQUFPOzs7Ozs7SUFBUCxVQUFXLFdBQW1CLEVBQUUsT0FBUTtRQUN0QyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3BGOzs7Ozs7OztJQUVELG9DQUFLOzs7Ozs7O0lBQUwsVUFBUyxXQUFtQixFQUFFLElBQWdCLEVBQUUsT0FBWTtRQUFaLHdCQUFBLEVBQUEsWUFBWTtRQUMxRCxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztLQUN4Rjs7Ozs7Ozs7SUFFRCxtQ0FBSTs7Ozs7OztJQUFKLFVBQVEsV0FBbUIsRUFBRSxJQUFnQixFQUFFLE9BQVk7UUFBWix3QkFBQSxFQUFBLFlBQVk7UUFDekQsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7S0FDdkY7Ozs7Ozs7O0lBRUQscUNBQU07Ozs7Ozs7SUFBTixVQUFVLFdBQW1CLEVBQUUsSUFBZ0IsRUFBRSxPQUFZO1FBQVosd0JBQUEsRUFBQSxZQUFZO1FBQzNELE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3RGOzs7OztJQUVELHlDQUFVOzs7O0lBQVYsVUFBVyxPQUFPO1FBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO0tBQzFCOzs7OztJQUdPLGtEQUFtQjs7OztjQUFDLFdBQW1CO1FBQzdDLEVBQUUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzNCLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQztTQUNyQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQztTQUMzQzs7O2dCQTNDSixVQUFVOzs7O2dCQUpILFVBQVU7OytCQURsQjs7U0FNYSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtIdHRwQ2xpZW50fSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge0h0dHBFdmVudH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwL3NyYy9yZXNwb25zZVwiO1xuaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tIFwicnhqc1wiO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgVXJsSHR0cENsaWVudFNlcnZpY2Uge1xuICBwcml2YXRlIF9iYXNlX2FwaTogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgX2h0dHBDbGllbnQ6IEh0dHBDbGllbnRcbiAgKSB7fVxuXG4gIGdldDxUPihyZXNvdXJjZVVybDogc3RyaW5nLCBvcHRpb25zID0ge30pOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICByZXR1cm4gdGhpcy5faHR0cENsaWVudC5nZXQ8VD4odGhpcy5fZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsKSwgb3B0aW9ucyk7XG4gIH1cblxuICAvLyBub2luc3BlY3Rpb24gSlNVbnVzZWRHbG9iYWxTeW1ib2xzXG4gIGRlbGV0ZTxUPihyZXNvdXJjZVVybDogc3RyaW5nLCBvcHRpb25zPyk6IE9ic2VydmFibGU8SHR0cEV2ZW50PFQ+PiB7XG4gICAgcmV0dXJuIHRoaXMuX2h0dHBDbGllbnQuZGVsZXRlPFQ+KHRoaXMuX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybCksIG9wdGlvbnMpO1xuICB9XG5cbiAgb3B0aW9uczxUPihyZXNvdXJjZVVybDogc3RyaW5nLCBvcHRpb25zPyk6IE9ic2VydmFibGU8SHR0cEV2ZW50PFQ+PiB7XG4gICAgcmV0dXJuIHRoaXMuX2h0dHBDbGllbnQub3B0aW9uczxUPih0aGlzLl9nZXRGdWxsUmVzb3VyY2VVcmwocmVzb3VyY2VVcmwpLCBvcHRpb25zKTtcbiAgfVxuXG4gIHBhdGNoPFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIGJvZHk6IGFueSB8IG51bGwsIG9wdGlvbnMgPSB7fSk6IE9ic2VydmFibGU8VD4ge1xuICAgIHJldHVybiB0aGlzLl9odHRwQ2xpZW50LnBhdGNoPFQ+KHRoaXMuX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybCksIGJvZHksIG9wdGlvbnMpO1xuICB9XG5cbiAgcG9zdDxUPihyZXNvdXJjZVVybDogc3RyaW5nLCBib2R5OiBhbnkgfCBudWxsLCBvcHRpb25zID0ge30pOiBPYnNlcnZhYmxlPFQ+IHtcbiAgICByZXR1cm4gdGhpcy5faHR0cENsaWVudC5wb3N0PFQ+KHRoaXMuX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybCksIGJvZHksIG9wdGlvbnMpO1xuICB9XG5cbiAgdXBkYXRlPFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIGJvZHk6IGFueSB8IG51bGwsIG9wdGlvbnMgPSB7fSk6IE9ic2VydmFibGU8VD4ge1xuICAgIHJldHVybiB0aGlzLl9odHRwQ2xpZW50LnB1dDxUPih0aGlzLl9nZXRGdWxsUmVzb3VyY2VVcmwocmVzb3VyY2VVcmwpLCBib2R5LCBvcHRpb25zKTtcbiAgfVxuXG4gIHNldEJhc2VBUEkoYmFzZUFQSSk6IHZvaWQge1xuICAgIHRoaXMuX2Jhc2VfYXBpID0gYmFzZUFQSTtcbiAgfVxuXG4gIC8qUHJpdmF0ZSBNZXRob2RzKi9cbiAgcHJpdmF0ZSBfZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsOiBzdHJpbmcpIHtcbiAgICBpZiAocmVzb3VyY2VVcmxbMF0gPT09ICcvJykge1xuICAgICAgcmV0dXJuIHRoaXMuX2Jhc2VfYXBpICsgcmVzb3VyY2VVcmw7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB0aGlzLl9iYXNlX2FwaSArICcvJyArIHJlc291cmNlVXJsO1xuICAgIH1cbiAgfVxufVxuIl19