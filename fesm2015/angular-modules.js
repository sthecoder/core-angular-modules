import { Directive, ElementRef, NgModule, Component, HostBinding, Input, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpClientModule } from '@angular/common/http';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AutofocusDirective {
    /**
     * @param {?} _elementRef
     */
    constructor(_elementRef) {
        this._elementRef = _elementRef;
        this._el = this._elementRef.nativeElement;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this._el.focus();
    }
}
AutofocusDirective.decorators = [
    { type: Directive, args: [{
                selector: '[acAutofocus]'
            },] },
];
/** @nocollapse */
AutofocusDirective.ctorParameters = () => [
    { type: ElementRef, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class AutofocusModule {
}
AutofocusModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [AutofocusDirective],
                exports: [AutofocusDirective]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class CollapseComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
CollapseComponent.decorators = [
    { type: Component, args: [{
                selector: '[acCollapse]',
                template: `<ng-content></ng-content>`,
                animations: [
                    trigger('collapse', [
                        state('0', style({ height: '0', opacity: '0', overflow: 'auto' })),
                        state('1', style({ height: '*', opacity: '1' })),
                        transition('0 => 1', animate('250ms ease-in')),
                        transition('1 => 0', animate('250ms ease-out'))
                    ])
                ]
            },] },
];
/** @nocollapse */
CollapseComponent.ctorParameters = () => [];
CollapseComponent.propDecorators = {
    "acCollapse": [{ type: HostBinding, args: ['@collapse',] }, { type: Input },],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class CollapseModule {
}
CollapseModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    BrowserAnimationsModule
                ],
                declarations: [CollapseComponent],
                exports: [CollapseComponent]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UrlHttpClientService {
    /**
     * @param {?} _httpClient
     */
    constructor(_httpClient) {
        this._httpClient = _httpClient;
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    get(resourceUrl, options = {}) {
        return this._httpClient.get(this._getFullResourceUrl(resourceUrl), options);
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    delete(resourceUrl, options) {
        return this._httpClient.delete(this._getFullResourceUrl(resourceUrl), options);
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    options(resourceUrl, options) {
        return this._httpClient.options(this._getFullResourceUrl(resourceUrl), options);
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    patch(resourceUrl, body, options = {}) {
        return this._httpClient.patch(this._getFullResourceUrl(resourceUrl), body, options);
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    post(resourceUrl, body, options = {}) {
        return this._httpClient.post(this._getFullResourceUrl(resourceUrl), body, options);
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    update(resourceUrl, body, options = {}) {
        return this._httpClient.put(this._getFullResourceUrl(resourceUrl), body, options);
    }
    /**
     * @param {?} baseAPI
     * @return {?}
     */
    setBaseAPI(baseAPI) {
        this._base_api = baseAPI;
    }
    /**
     * @param {?} resourceUrl
     * @return {?}
     */
    _getFullResourceUrl(resourceUrl) {
        if (resourceUrl[0] === '/') {
            return this._base_api + resourceUrl;
        }
        else {
            return this._base_api + '/' + resourceUrl;
        }
    }
}
UrlHttpClientService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
UrlHttpClientService.ctorParameters = () => [
    { type: HttpClient, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class UrlHttpClientModule {
}
UrlHttpClientModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    HttpClientModule
                ],
                declarations: [],
                providers: [
                    UrlHttpClientService
                ]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { AutofocusModule, AutofocusDirective, CollapseModule, CollapseComponent, UrlHttpClientModule, UrlHttpClientService };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1tb2R1bGVzLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9hbmd1bGFyLW1vZHVsZXMvc3JjL2xpYi9tb2R1bGVzL2F1dG9mb2N1cy9hdXRvZm9jdXMuZGlyZWN0aXZlLnRzIiwibmc6Ly9hbmd1bGFyLW1vZHVsZXMvc3JjL2xpYi9tb2R1bGVzL2F1dG9mb2N1cy9hdXRvZm9jdXMubW9kdWxlLnRzIiwibmc6Ly9hbmd1bGFyLW1vZHVsZXMvc3JjL2xpYi9tb2R1bGVzL2NvbGxhcHNlL2NvbGxhcHNlL2NvbGxhcHNlLmNvbXBvbmVudC50cyIsIm5nOi8vYW5ndWxhci1tb2R1bGVzL3NyYy9saWIvbW9kdWxlcy9jb2xsYXBzZS9jb2xsYXBzZS5tb2R1bGUudHMiLCJuZzovL2FuZ3VsYXItbW9kdWxlcy9zcmMvbGliL21vZHVsZXMvdXJsLWh0dHAtY2xpZW50L3VybC1odHRwLWNsaWVudC5zZXJ2aWNlLnRzIiwibmc6Ly9hbmd1bGFyLW1vZHVsZXMvc3JjL2xpYi9tb2R1bGVzL3VybC1odHRwLWNsaWVudC91cmwtaHR0cC1jbGllbnQubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFmdGVyVmlld0luaXQsIERpcmVjdGl2ZSwgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbYWNBdXRvZm9jdXNdJ1xufSlcbmV4cG9ydCBjbGFzcyBBdXRvZm9jdXNEaXJlY3RpdmUgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcbiAgcHJpdmF0ZSBfZWw6IGFueTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9lbGVtZW50UmVmOiBFbGVtZW50UmVmKSB7XG4gICAgdGhpcy5fZWwgPSB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgdGhpcy5fZWwuZm9jdXMoKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbmltcG9ydCB7IEF1dG9mb2N1c0RpcmVjdGl2ZSB9IGZyb20gJy4vYXV0b2ZvY3VzLmRpcmVjdGl2ZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbQXV0b2ZvY3VzRGlyZWN0aXZlXSxcbiAgZXhwb3J0czogW0F1dG9mb2N1c0RpcmVjdGl2ZV1cbn0pXG5leHBvcnQgY2xhc3MgQXV0b2ZvY3VzTW9kdWxlIHt9XG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSG9zdEJpbmRpbmcsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHthbmltYXRlLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIHRyaWdnZXJ9IGZyb20gXCJAYW5ndWxhci9hbmltYXRpb25zXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ1thY0NvbGxhcHNlXScsXG4gIHRlbXBsYXRlOiBgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PmAsXG4gIGFuaW1hdGlvbnM6IFtcbiAgICB0cmlnZ2VyKCdjb2xsYXBzZScsIFtcbiAgICAgIHN0YXRlKCcwJywgc3R5bGUoeyBoZWlnaHQ6ICcwJywgb3BhY2l0eTogJzAnLCBvdmVyZmxvdzogJ2F1dG8nIH0pKSxcbiAgICAgIHN0YXRlKCcxJywgc3R5bGUoeyBoZWlnaHQ6ICcqJywgb3BhY2l0eTogJzEnIH0pKSxcbiAgICAgIHRyYW5zaXRpb24oJzAgPT4gMScsIGFuaW1hdGUoJzI1MG1zIGVhc2UtaW4nKSksXG4gICAgICB0cmFuc2l0aW9uKCcxID0+IDAnLCBhbmltYXRlKCcyNTBtcyBlYXNlLW91dCcpKVxuICAgIF0pXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgQ29sbGFwc2VDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASG9zdEJpbmRpbmcoJ0Bjb2xsYXBzZScpXG4gIEBJbnB1dCgpXG4gIGFjQ29sbGFwc2U6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG5nT25Jbml0KCkge31cbn1cbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgQ29sbGFwc2VDb21wb25lbnQgfSBmcm9tICcuL2NvbGxhcHNlL2NvbGxhcHNlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBCcm93c2VyQW5pbWF0aW9uc01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyL2FuaW1hdGlvbnNcIjtcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBCcm93c2VyQW5pbWF0aW9uc01vZHVsZVxuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtDb2xsYXBzZUNvbXBvbmVudF0sXG4gIGV4cG9ydHM6IFtDb2xsYXBzZUNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgQ29sbGFwc2VNb2R1bGUge31cbiIsImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0h0dHBDbGllbnR9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7SHR0cEV2ZW50fSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHAvc3JjL3Jlc3BvbnNlXCI7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gXCJyeGpzXCI7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBVcmxIdHRwQ2xpZW50U2VydmljZSB7XG4gIHByaXZhdGUgX2Jhc2VfYXBpOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBfaHR0cENsaWVudDogSHR0cENsaWVudFxuICApIHt9XG5cbiAgZ2V0PFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIG9wdGlvbnMgPSB7fSk6IE9ic2VydmFibGU8VD4ge1xuICAgIHJldHVybiB0aGlzLl9odHRwQ2xpZW50LmdldDxUPih0aGlzLl9nZXRGdWxsUmVzb3VyY2VVcmwocmVzb3VyY2VVcmwpLCBvcHRpb25zKTtcbiAgfVxuXG4gIC8vIG5vaW5zcGVjdGlvbiBKU1VudXNlZEdsb2JhbFN5bWJvbHNcbiAgZGVsZXRlPFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIG9wdGlvbnM/KTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8VD4+IHtcbiAgICByZXR1cm4gdGhpcy5faHR0cENsaWVudC5kZWxldGU8VD4odGhpcy5fZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsKSwgb3B0aW9ucyk7XG4gIH1cblxuICBvcHRpb25zPFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIG9wdGlvbnM/KTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8VD4+IHtcbiAgICByZXR1cm4gdGhpcy5faHR0cENsaWVudC5vcHRpb25zPFQ+KHRoaXMuX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybCksIG9wdGlvbnMpO1xuICB9XG5cbiAgcGF0Y2g8VD4ocmVzb3VyY2VVcmw6IHN0cmluZywgYm9keTogYW55IHwgbnVsbCwgb3B0aW9ucyA9IHt9KTogT2JzZXJ2YWJsZTxUPiB7XG4gICAgcmV0dXJuIHRoaXMuX2h0dHBDbGllbnQucGF0Y2g8VD4odGhpcy5fZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsKSwgYm9keSwgb3B0aW9ucyk7XG4gIH1cblxuICBwb3N0PFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIGJvZHk6IGFueSB8IG51bGwsIG9wdGlvbnMgPSB7fSk6IE9ic2VydmFibGU8VD4ge1xuICAgIHJldHVybiB0aGlzLl9odHRwQ2xpZW50LnBvc3Q8VD4odGhpcy5fZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsKSwgYm9keSwgb3B0aW9ucyk7XG4gIH1cblxuICB1cGRhdGU8VD4ocmVzb3VyY2VVcmw6IHN0cmluZywgYm9keTogYW55IHwgbnVsbCwgb3B0aW9ucyA9IHt9KTogT2JzZXJ2YWJsZTxUPiB7XG4gICAgcmV0dXJuIHRoaXMuX2h0dHBDbGllbnQucHV0PFQ+KHRoaXMuX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybCksIGJvZHksIG9wdGlvbnMpO1xuICB9XG5cbiAgc2V0QmFzZUFQSShiYXNlQVBJKTogdm9pZCB7XG4gICAgdGhpcy5fYmFzZV9hcGkgPSBiYXNlQVBJO1xuICB9XG5cbiAgLypQcml2YXRlIE1ldGhvZHMqL1xuICBwcml2YXRlIF9nZXRGdWxsUmVzb3VyY2VVcmwocmVzb3VyY2VVcmw6IHN0cmluZykge1xuICAgIGlmIChyZXNvdXJjZVVybFswXSA9PT0gJy8nKSB7XG4gICAgICByZXR1cm4gdGhpcy5fYmFzZV9hcGkgKyByZXNvdXJjZVVybDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHRoaXMuX2Jhc2VfYXBpICsgJy8nICsgcmVzb3VyY2VVcmw7XG4gICAgfVxuICB9XG59XG4iLCJpbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7SHR0cENsaWVudE1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XG5pbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7VXJsSHR0cENsaWVudFNlcnZpY2V9IGZyb20gJy4vdXJsLWh0dHAtY2xpZW50LnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIEh0dHBDbGllbnRNb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAgVXJsSHR0cENsaWVudFNlcnZpY2VcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBVcmxIdHRwQ2xpZW50TW9kdWxlIHt9XG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBOzs7O0lBUUUsWUFBb0IsV0FBdUI7UUFBdkIsZ0JBQVcsR0FBWCxXQUFXLENBQVk7UUFDekMsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQztLQUMzQzs7OztJQUVELGVBQWU7UUFDYixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO0tBQ2xCOzs7WUFaRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGVBQWU7YUFDMUI7Ozs7WUFKa0MsVUFBVTs7Ozs7OztBQ0E3Qzs7O1lBS0MsUUFBUSxTQUFDO2dCQUNSLE9BQU8sRUFBRTtvQkFDUCxZQUFZO2lCQUNiO2dCQUNELFlBQVksRUFBRSxDQUFDLGtCQUFrQixDQUFDO2dCQUNsQyxPQUFPLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQzthQUM5Qjs7Ozs7OztBQ1hEO0lBb0JFLGlCQUFnQjs7OztJQUVoQixRQUFRLE1BQUs7OztZQW5CZCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLFFBQVEsRUFBRSwyQkFBMkI7Z0JBQ3JDLFVBQVUsRUFBRTtvQkFDVixPQUFPLENBQUMsVUFBVSxFQUFFO3dCQUNsQixLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQzt3QkFDbEUsS0FBSyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNoRCxVQUFVLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxlQUFlLENBQUMsQ0FBQzt3QkFDOUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztxQkFDaEQsQ0FBQztpQkFDSDthQUNGOzs7OzsyQkFFRSxXQUFXLFNBQUMsV0FBVyxjQUN2QixLQUFLOzs7Ozs7O0FDakJSOzs7WUFLQyxRQUFRLFNBQUM7Z0JBQ1IsT0FBTyxFQUFFO29CQUNQLFlBQVk7b0JBQ1osdUJBQXVCO2lCQUN4QjtnQkFDRCxZQUFZLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztnQkFDakMsT0FBTyxFQUFFLENBQUMsaUJBQWlCLENBQUM7YUFDN0I7Ozs7Ozs7QUNaRDs7OztJQVNFLFlBQ1U7UUFBQSxnQkFBVyxHQUFYLFdBQVc7S0FDakI7Ozs7Ozs7SUFFSixHQUFHLENBQUksV0FBbUIsRUFBRSxPQUFPLEdBQUcsRUFBRTtRQUN0QyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztLQUNoRjs7Ozs7OztJQUdELE1BQU0sQ0FBSSxXQUFtQixFQUFFLE9BQVE7UUFDckMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7S0FDbkY7Ozs7Ozs7SUFFRCxPQUFPLENBQUksV0FBbUIsRUFBRSxPQUFRO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3BGOzs7Ozs7OztJQUVELEtBQUssQ0FBSSxXQUFtQixFQUFFLElBQWdCLEVBQUUsT0FBTyxHQUFHLEVBQUU7UUFDMUQsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3hGOzs7Ozs7OztJQUVELElBQUksQ0FBSSxXQUFtQixFQUFFLElBQWdCLEVBQUUsT0FBTyxHQUFHLEVBQUU7UUFDekQsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3ZGOzs7Ozs7OztJQUVELE1BQU0sQ0FBSSxXQUFtQixFQUFFLElBQWdCLEVBQUUsT0FBTyxHQUFHLEVBQUU7UUFDM0QsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3RGOzs7OztJQUVELFVBQVUsQ0FBQyxPQUFPO1FBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO0tBQzFCOzs7OztJQUdPLG1CQUFtQixDQUFDLFdBQW1CO1FBQzdDLElBQUksV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTtZQUMxQixPQUFPLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDO1NBQ3JDO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQztTQUMzQzs7OztZQTNDSixVQUFVOzs7O1lBSkgsVUFBVTs7Ozs7OztBQ0RsQjs7O1lBS0MsUUFBUSxTQUFDO2dCQUNSLE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLGdCQUFnQjtpQkFDakI7Z0JBQ0QsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLFNBQVMsRUFBRTtvQkFDVCxvQkFBb0I7aUJBQ3JCO2FBQ0Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9