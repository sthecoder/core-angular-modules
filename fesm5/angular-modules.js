import { Directive, ElementRef, NgModule, Component, HostBinding, Input, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpClientModule } from '@angular/common/http';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AutofocusDirective = /** @class */ (function () {
    function AutofocusDirective(_elementRef) {
        this._elementRef = _elementRef;
        this._el = this._elementRef.nativeElement;
    }
    /**
     * @return {?}
     */
    AutofocusDirective.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        this._el.focus();
    };
    AutofocusDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[acAutofocus]'
                },] },
    ];
    /** @nocollapse */
    AutofocusDirective.ctorParameters = function () { return [
        { type: ElementRef, },
    ]; };
    return AutofocusDirective;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var AutofocusModule = /** @class */ (function () {
    function AutofocusModule() {
    }
    AutofocusModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule
                    ],
                    declarations: [AutofocusDirective],
                    exports: [AutofocusDirective]
                },] },
    ];
    return AutofocusModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var CollapseComponent = /** @class */ (function () {
    function CollapseComponent() {
    }
    /**
     * @return {?}
     */
    CollapseComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    CollapseComponent.decorators = [
        { type: Component, args: [{
                    selector: '[acCollapse]',
                    template: "<ng-content></ng-content>",
                    animations: [
                        trigger('collapse', [
                            state('0', style({ height: '0', opacity: '0', overflow: 'auto' })),
                            state('1', style({ height: '*', opacity: '1' })),
                            transition('0 => 1', animate('250ms ease-in')),
                            transition('1 => 0', animate('250ms ease-out'))
                        ])
                    ]
                },] },
    ];
    /** @nocollapse */
    CollapseComponent.ctorParameters = function () { return []; };
    CollapseComponent.propDecorators = {
        "acCollapse": [{ type: HostBinding, args: ['@collapse',] }, { type: Input },],
    };
    return CollapseComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var CollapseModule = /** @class */ (function () {
    function CollapseModule() {
    }
    CollapseModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        BrowserAnimationsModule
                    ],
                    declarations: [CollapseComponent],
                    exports: [CollapseComponent]
                },] },
    ];
    return CollapseModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var UrlHttpClientService = /** @class */ (function () {
    function UrlHttpClientService(_httpClient) {
        this._httpClient = _httpClient;
    }
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.get = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, options) {
        if (options === void 0) { options = {}; }
        return this._httpClient.get(this._getFullResourceUrl(resourceUrl), options);
    };
    // noinspection JSUnusedGlobalSymbols
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.delete = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, options) {
        return this._httpClient.delete(this._getFullResourceUrl(resourceUrl), options);
    };
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.options = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, options) {
        return this._httpClient.options(this._getFullResourceUrl(resourceUrl), options);
    };
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.patch = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, body, options) {
        if (options === void 0) { options = {}; }
        return this._httpClient.patch(this._getFullResourceUrl(resourceUrl), body, options);
    };
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.post = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, body, options) {
        if (options === void 0) { options = {}; }
        return this._httpClient.post(this._getFullResourceUrl(resourceUrl), body, options);
    };
    /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    UrlHttpClientService.prototype.update = /**
     * @template T
     * @param {?} resourceUrl
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    function (resourceUrl, body, options) {
        if (options === void 0) { options = {}; }
        return this._httpClient.put(this._getFullResourceUrl(resourceUrl), body, options);
    };
    /**
     * @param {?} baseAPI
     * @return {?}
     */
    UrlHttpClientService.prototype.setBaseAPI = /**
     * @param {?} baseAPI
     * @return {?}
     */
    function (baseAPI) {
        this._base_api = baseAPI;
    };
    /**
     * @param {?} resourceUrl
     * @return {?}
     */
    UrlHttpClientService.prototype._getFullResourceUrl = /**
     * @param {?} resourceUrl
     * @return {?}
     */
    function (resourceUrl) {
        if (resourceUrl[0] === '/') {
            return this._base_api + resourceUrl;
        }
        else {
            return this._base_api + '/' + resourceUrl;
        }
    };
    UrlHttpClientService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    UrlHttpClientService.ctorParameters = function () { return [
        { type: HttpClient, },
    ]; };
    return UrlHttpClientService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var UrlHttpClientModule = /** @class */ (function () {
    function UrlHttpClientModule() {
    }
    UrlHttpClientModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        HttpClientModule
                    ],
                    declarations: [],
                    providers: [
                        UrlHttpClientService
                    ]
                },] },
    ];
    return UrlHttpClientModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

export { AutofocusModule, AutofocusDirective, CollapseModule, CollapseComponent, UrlHttpClientModule, UrlHttpClientService };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1tb2R1bGVzLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9hbmd1bGFyLW1vZHVsZXMvc3JjL2xpYi9tb2R1bGVzL2F1dG9mb2N1cy9hdXRvZm9jdXMuZGlyZWN0aXZlLnRzIiwibmc6Ly9hbmd1bGFyLW1vZHVsZXMvc3JjL2xpYi9tb2R1bGVzL2F1dG9mb2N1cy9hdXRvZm9jdXMubW9kdWxlLnRzIiwibmc6Ly9hbmd1bGFyLW1vZHVsZXMvc3JjL2xpYi9tb2R1bGVzL2NvbGxhcHNlL2NvbGxhcHNlL2NvbGxhcHNlLmNvbXBvbmVudC50cyIsIm5nOi8vYW5ndWxhci1tb2R1bGVzL3NyYy9saWIvbW9kdWxlcy9jb2xsYXBzZS9jb2xsYXBzZS5tb2R1bGUudHMiLCJuZzovL2FuZ3VsYXItbW9kdWxlcy9zcmMvbGliL21vZHVsZXMvdXJsLWh0dHAtY2xpZW50L3VybC1odHRwLWNsaWVudC5zZXJ2aWNlLnRzIiwibmc6Ly9hbmd1bGFyLW1vZHVsZXMvc3JjL2xpYi9tb2R1bGVzL3VybC1odHRwLWNsaWVudC91cmwtaHR0cC1jbGllbnQubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFmdGVyVmlld0luaXQsIERpcmVjdGl2ZSwgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbYWNBdXRvZm9jdXNdJ1xufSlcbmV4cG9ydCBjbGFzcyBBdXRvZm9jdXNEaXJlY3RpdmUgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcbiAgcHJpdmF0ZSBfZWw6IGFueTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9lbGVtZW50UmVmOiBFbGVtZW50UmVmKSB7XG4gICAgdGhpcy5fZWwgPSB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgdGhpcy5fZWwuZm9jdXMoKTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbmltcG9ydCB7IEF1dG9mb2N1c0RpcmVjdGl2ZSB9IGZyb20gJy4vYXV0b2ZvY3VzLmRpcmVjdGl2ZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbQXV0b2ZvY3VzRGlyZWN0aXZlXSxcbiAgZXhwb3J0czogW0F1dG9mb2N1c0RpcmVjdGl2ZV1cbn0pXG5leHBvcnQgY2xhc3MgQXV0b2ZvY3VzTW9kdWxlIHt9XG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSG9zdEJpbmRpbmcsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHthbmltYXRlLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIHRyaWdnZXJ9IGZyb20gXCJAYW5ndWxhci9hbmltYXRpb25zXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ1thY0NvbGxhcHNlXScsXG4gIHRlbXBsYXRlOiBgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PmAsXG4gIGFuaW1hdGlvbnM6IFtcbiAgICB0cmlnZ2VyKCdjb2xsYXBzZScsIFtcbiAgICAgIHN0YXRlKCcwJywgc3R5bGUoeyBoZWlnaHQ6ICcwJywgb3BhY2l0eTogJzAnLCBvdmVyZmxvdzogJ2F1dG8nIH0pKSxcbiAgICAgIHN0YXRlKCcxJywgc3R5bGUoeyBoZWlnaHQ6ICcqJywgb3BhY2l0eTogJzEnIH0pKSxcbiAgICAgIHRyYW5zaXRpb24oJzAgPT4gMScsIGFuaW1hdGUoJzI1MG1zIGVhc2UtaW4nKSksXG4gICAgICB0cmFuc2l0aW9uKCcxID0+IDAnLCBhbmltYXRlKCcyNTBtcyBlYXNlLW91dCcpKVxuICAgIF0pXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgQ29sbGFwc2VDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASG9zdEJpbmRpbmcoJ0Bjb2xsYXBzZScpXG4gIEBJbnB1dCgpXG4gIGFjQ29sbGFwc2U6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG5nT25Jbml0KCkge31cbn1cbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgQ29sbGFwc2VDb21wb25lbnQgfSBmcm9tICcuL2NvbGxhcHNlL2NvbGxhcHNlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBCcm93c2VyQW5pbWF0aW9uc01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyL2FuaW1hdGlvbnNcIjtcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBCcm93c2VyQW5pbWF0aW9uc01vZHVsZVxuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtDb2xsYXBzZUNvbXBvbmVudF0sXG4gIGV4cG9ydHM6IFtDb2xsYXBzZUNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgQ29sbGFwc2VNb2R1bGUge31cbiIsImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0h0dHBDbGllbnR9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7SHR0cEV2ZW50fSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHAvc3JjL3Jlc3BvbnNlXCI7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gXCJyeGpzXCI7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBVcmxIdHRwQ2xpZW50U2VydmljZSB7XG4gIHByaXZhdGUgX2Jhc2VfYXBpOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBfaHR0cENsaWVudDogSHR0cENsaWVudFxuICApIHt9XG5cbiAgZ2V0PFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIG9wdGlvbnMgPSB7fSk6IE9ic2VydmFibGU8VD4ge1xuICAgIHJldHVybiB0aGlzLl9odHRwQ2xpZW50LmdldDxUPih0aGlzLl9nZXRGdWxsUmVzb3VyY2VVcmwocmVzb3VyY2VVcmwpLCBvcHRpb25zKTtcbiAgfVxuXG4gIC8vIG5vaW5zcGVjdGlvbiBKU1VudXNlZEdsb2JhbFN5bWJvbHNcbiAgZGVsZXRlPFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIG9wdGlvbnM/KTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8VD4+IHtcbiAgICByZXR1cm4gdGhpcy5faHR0cENsaWVudC5kZWxldGU8VD4odGhpcy5fZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsKSwgb3B0aW9ucyk7XG4gIH1cblxuICBvcHRpb25zPFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIG9wdGlvbnM/KTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8VD4+IHtcbiAgICByZXR1cm4gdGhpcy5faHR0cENsaWVudC5vcHRpb25zPFQ+KHRoaXMuX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybCksIG9wdGlvbnMpO1xuICB9XG5cbiAgcGF0Y2g8VD4ocmVzb3VyY2VVcmw6IHN0cmluZywgYm9keTogYW55IHwgbnVsbCwgb3B0aW9ucyA9IHt9KTogT2JzZXJ2YWJsZTxUPiB7XG4gICAgcmV0dXJuIHRoaXMuX2h0dHBDbGllbnQucGF0Y2g8VD4odGhpcy5fZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsKSwgYm9keSwgb3B0aW9ucyk7XG4gIH1cblxuICBwb3N0PFQ+KHJlc291cmNlVXJsOiBzdHJpbmcsIGJvZHk6IGFueSB8IG51bGwsIG9wdGlvbnMgPSB7fSk6IE9ic2VydmFibGU8VD4ge1xuICAgIHJldHVybiB0aGlzLl9odHRwQ2xpZW50LnBvc3Q8VD4odGhpcy5fZ2V0RnVsbFJlc291cmNlVXJsKHJlc291cmNlVXJsKSwgYm9keSwgb3B0aW9ucyk7XG4gIH1cblxuICB1cGRhdGU8VD4ocmVzb3VyY2VVcmw6IHN0cmluZywgYm9keTogYW55IHwgbnVsbCwgb3B0aW9ucyA9IHt9KTogT2JzZXJ2YWJsZTxUPiB7XG4gICAgcmV0dXJuIHRoaXMuX2h0dHBDbGllbnQucHV0PFQ+KHRoaXMuX2dldEZ1bGxSZXNvdXJjZVVybChyZXNvdXJjZVVybCksIGJvZHksIG9wdGlvbnMpO1xuICB9XG5cbiAgc2V0QmFzZUFQSShiYXNlQVBJKTogdm9pZCB7XG4gICAgdGhpcy5fYmFzZV9hcGkgPSBiYXNlQVBJO1xuICB9XG5cbiAgLypQcml2YXRlIE1ldGhvZHMqL1xuICBwcml2YXRlIF9nZXRGdWxsUmVzb3VyY2VVcmwocmVzb3VyY2VVcmw6IHN0cmluZykge1xuICAgIGlmIChyZXNvdXJjZVVybFswXSA9PT0gJy8nKSB7XG4gICAgICByZXR1cm4gdGhpcy5fYmFzZV9hcGkgKyByZXNvdXJjZVVybDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHRoaXMuX2Jhc2VfYXBpICsgJy8nICsgcmVzb3VyY2VVcmw7XG4gICAgfVxuICB9XG59XG4iLCJpbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7SHR0cENsaWVudE1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XG5pbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7VXJsSHR0cENsaWVudFNlcnZpY2V9IGZyb20gJy4vdXJsLWh0dHAtY2xpZW50LnNlcnZpY2UnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIEh0dHBDbGllbnRNb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAgVXJsSHR0cENsaWVudFNlcnZpY2VcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBVcmxIdHRwQ2xpZW50TW9kdWxlIHt9XG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBO0lBUUUsNEJBQW9CLFdBQXVCO1FBQXZCLGdCQUFXLEdBQVgsV0FBVyxDQUFZO1FBQ3pDLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7S0FDM0M7Ozs7SUFFRCw0Q0FBZTs7O0lBQWY7UUFDRSxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO0tBQ2xCOztnQkFaRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGVBQWU7aUJBQzFCOzs7O2dCQUprQyxVQUFVOzs2QkFBN0M7Ozs7Ozs7QUNBQTs7OztnQkFLQyxRQUFRLFNBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7cUJBQ2I7b0JBQ0QsWUFBWSxFQUFFLENBQUMsa0JBQWtCLENBQUM7b0JBQ2xDLE9BQU8sRUFBRSxDQUFDLGtCQUFrQixDQUFDO2lCQUM5Qjs7MEJBWEQ7Ozs7Ozs7QUNBQTtJQW9CRTtLQUFnQjs7OztJQUVoQixvQ0FBUTs7O0lBQVIsZUFBYTs7Z0JBbkJkLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsY0FBYztvQkFDeEIsUUFBUSxFQUFFLDJCQUEyQjtvQkFDckMsVUFBVSxFQUFFO3dCQUNWLE9BQU8sQ0FBQyxVQUFVLEVBQUU7NEJBQ2xCLEtBQUssQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDOzRCQUNsRSxLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ2hELFVBQVUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDOzRCQUM5QyxVQUFVLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO3lCQUNoRCxDQUFDO3FCQUNIO2lCQUNGOzs7OzsrQkFFRSxXQUFXLFNBQUMsV0FBVyxjQUN2QixLQUFLOzs0QkFqQlI7Ozs7Ozs7QUNBQTs7OztnQkFLQyxRQUFRLFNBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBQ1osdUJBQXVCO3FCQUN4QjtvQkFDRCxZQUFZLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztvQkFDakMsT0FBTyxFQUFFLENBQUMsaUJBQWlCLENBQUM7aUJBQzdCOzt5QkFaRDs7Ozs7OztBQ0FBO0lBU0UsOEJBQ1U7UUFBQSxnQkFBVyxHQUFYLFdBQVc7S0FDakI7Ozs7Ozs7SUFFSixrQ0FBRzs7Ozs7O0lBQUgsVUFBTyxXQUFtQixFQUFFLE9BQVk7UUFBWix3QkFBQSxFQUFBLFlBQVk7UUFDdEMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7S0FDaEY7Ozs7Ozs7O0lBR0QscUNBQU07Ozs7OztJQUFOLFVBQVUsV0FBbUIsRUFBRSxPQUFRO1FBQ3JDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ25GOzs7Ozs7O0lBRUQsc0NBQU87Ozs7OztJQUFQLFVBQVcsV0FBbUIsRUFBRSxPQUFRO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3BGOzs7Ozs7OztJQUVELG9DQUFLOzs7Ozs7O0lBQUwsVUFBUyxXQUFtQixFQUFFLElBQWdCLEVBQUUsT0FBWTtRQUFaLHdCQUFBLEVBQUEsWUFBWTtRQUMxRCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7S0FDeEY7Ozs7Ozs7O0lBRUQsbUNBQUk7Ozs7Ozs7SUFBSixVQUFRLFdBQW1CLEVBQUUsSUFBZ0IsRUFBRSxPQUFZO1FBQVosd0JBQUEsRUFBQSxZQUFZO1FBQ3pELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztLQUN2Rjs7Ozs7Ozs7SUFFRCxxQ0FBTTs7Ozs7OztJQUFOLFVBQVUsV0FBbUIsRUFBRSxJQUFnQixFQUFFLE9BQVk7UUFBWix3QkFBQSxFQUFBLFlBQVk7UUFDM0QsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3RGOzs7OztJQUVELHlDQUFVOzs7O0lBQVYsVUFBVyxPQUFPO1FBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO0tBQzFCOzs7OztJQUdPLGtEQUFtQjs7OztjQUFDLFdBQW1CO1FBQzdDLElBQUksV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTtZQUMxQixPQUFPLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDO1NBQ3JDO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQztTQUMzQzs7O2dCQTNDSixVQUFVOzs7O2dCQUpILFVBQVU7OytCQURsQjs7Ozs7OztBQ0FBOzs7O2dCQUtDLFFBQVEsU0FBQztvQkFDUixPQUFPLEVBQUU7d0JBQ1AsWUFBWTt3QkFDWixnQkFBZ0I7cUJBQ2pCO29CQUNELFlBQVksRUFBRSxFQUFFO29CQUNoQixTQUFTLEVBQUU7d0JBQ1Qsb0JBQW9CO3FCQUNyQjtpQkFDRjs7OEJBZEQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9