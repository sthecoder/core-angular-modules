import { AfterViewInit, ElementRef } from '@angular/core';
export declare class AutofocusDirective implements AfterViewInit {
    private _elementRef;
    private _el;
    constructor(_elementRef: ElementRef);
    ngAfterViewInit(): void;
}
