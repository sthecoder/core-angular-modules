import { HttpClient } from '@angular/common/http';
import { HttpEvent } from "@angular/common/http/src/response";
import { Observable } from "rxjs";
export declare class UrlHttpClientService {
    private _httpClient;
    private _base_api;
    constructor(_httpClient: HttpClient);
    get<T>(resourceUrl: string, options?: {}): Observable<T>;
    delete<T>(resourceUrl: string, options?: any): Observable<HttpEvent<T>>;
    options<T>(resourceUrl: string, options?: any): Observable<HttpEvent<T>>;
    patch<T>(resourceUrl: string, body: any | null, options?: {}): Observable<T>;
    post<T>(resourceUrl: string, body: any | null, options?: {}): Observable<T>;
    update<T>(resourceUrl: string, body: any | null, options?: {}): Observable<T>;
    setBaseAPI(baseAPI: any): void;
    private _getFullResourceUrl(resourceUrl);
}
